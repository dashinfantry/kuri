/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools"
import "FieldLayout.js" as FieldLayout

Item {
    id: root

    property int row
    property int column

    visible: FieldLayout.layout[root.row][root.column].enabled

    function update() {
        //TODO: find a way to check if the component really changed
        if(true) {//!loader.source.toString().endsWith(FieldLayout.layout[root.row][root.column].source)) {
            loader.sourceComponent = undefined; // this cause the loaded component to be destroyed
            loader.setSource(FieldLayout.layout[root.row][root.column].source, {});
        }
        loader.visible = FieldLayout.layout[root.row][root.column].enabled;
    }

    DelayedTrigger {
        id: editTimer
        anchors.fill: parent
        backgroundColor: RecordPageColorTheme.colors.secondaryHighlight
        opacity: Theme.highlightBackgroundOpacity

        onTrigger: pageStack.push(Qt.resolvedUrl("../pages/RecordPageFieldDialog.qml"), { "field": root })
    }

    Loader {
        id: loader
        anchors.fill: parent

        source: FieldLayout.layout[root.row][root.column].source

        onLoaded: {
            item.condensed = FieldLayout.layout[root.row][0].enabled && FieldLayout.layout[root.row][1].enabled
        }
    }
}
