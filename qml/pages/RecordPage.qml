/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../components"
import "../fields"
import "../tools"

import "../tools/SharedResources.js" as SharedResources
import "../tools/Thresholds.js" as Thresholds
import "../tools/JSTools.js" as JSTools
import "../tools/RecordPageDisplay.js" as RecordPageDisplay
import "../fields/FieldLayout.js" as FieldLayout

Page {
    id: page
    allowedOrientations: settings.recordPagePortrait ? Orientation.Portrait : Orientation.All

    //If pause and we have no data and the map is not big, going back is possible
    backNavigation: (!recorder.running && recorder.isEmpty && !map.maximized && !lockScreen.visible)

    property var displayColors: RecordPageColorTheme.colors

    Component.onCompleted: {
        RecordPageColorTheme.id = settings.displayMode;

        console.log("################################################################");
        console.log("#                                                              #");
        console.log("#                                                              #");
        console.log("#                     RecordPage loaded                        #");
        console.log("#                                                              #");
        console.log("#                                                              #");
        console.log("################################################################");
    }

    onStatusChanged: {
        switch (status) {
            case PageStatus.Inactive:
                // nothing to do, move along
                break;
            case PageStatus.Activating:
                recorder.vStartGPS();

                if (settings.useHRMservice) {
                    dbusHRM.call("start");
                }

                if (recorder.isEmpty === false) {
                    // add points to map if track was restored
                    for(var i = 0; i < recorder.points; i++) {
                        map.addMapPoint(recorder.trackPointAt(i), i);
                    }
                    map.updateTrack();
                }

                recorder.newTrackPoint.connect(newTrackPoint);
                recorder.currentPositionChanged.connect(map.updateCurrentPosition);

                //If this page is shown, prevent screen from going blank
                if (settings.disableScreenBlanking) {
                    fncEnableScreenBlank(true);
                }

                //TODO add pebble and HRM

                //Load threshold settings and convert them to JS array
                Thresholds.fncConvertSaveStringToArray(settings.thresholds);
                break;
            case PageStatus.Active:
                // nothing to do, move along
                break;
            case PageStatus.Deactivating:
                recorder.vEndGPS();

                if (settings.useHRMservice) {
                    dbusHRM.call("stop");
                }

                recorder.newTrackPoint.disconnect(newTrackPoint);
                recorder.currentPositionChanged.disconnect(map.updateCurrentPosition);

                if (settings.disableScreenBlanking) {
                    fncEnableScreenBlank(false);
                }
                break;
        }
    }

    function newTrackPoint(coordinate, iPointIndex) {

        map.addMapPoint(coordinate, iPointIndex);
        map.updateTrack();

        //Thresholds processing needs to be disabled if recording is paused
        if (recorder.pause)
            return;

        var sVoiceLanguage = "_en_male.wav";
        //check voice language and generate last part of audio filename
        if (settings.voiceLanguage === 0)        //english male
            sVoiceLanguage = "_en_male.wav";
        else if (settings.voiceLanguage === 1)   //german male
            sVoiceLanguage = "_de_male.wav";

        switch (Thresholds.fncCheckHRThresholds(sHeartRate)) {
            case 1: //normal
                fncPlaySound("audio/hr_normal" + sVoiceLanguage);
                break;
            case 2: //low
                fncPlaySound("audio/hr_toolow" + sVoiceLanguage);
                break;
            case 3: //high
                fncPlaySound("audio/hr_toohigh" + sVoiceLanguage);
                break;
        }

        switch (Thresholds.fncCheckPaceThresholds(recorder.pace.toFixed(1))) {
            case 1: //normal
                fncPlaySound("audio/pace_normal" + sVoiceLanguage);
                break;
            case 2: //low
                fncPlaySound("audio/pace_toolow" + sVoiceLanguage);
                break;
            case 3: //high
                fncPlaySound("audio/pace_toohigh" + sVoiceLanguage);
                break;
        }
    }

    function showSaveDialog()
    {
        //If autosave is active...
        if (settings.enableAutosave)
        {
            console.log("Autosaving workout");
            recorder.exportGpx(SharedResources.arrayLookupWorkoutTableByName[settings.workoutType].labeltext + " - " + recorder.startingDateTime + " - " + (recorder.distance/1000).toFixed(1) + "km", "");
            recorder.clearTrack();  // TODO: Make sure save was successful?

            //Mainpage must load history data to get this new workout in the list
            bLoadHistoryData = true;

            //We must return here to the mainpage.
            pageStack.pop(vMainPageObject, PageStackAction.Immediate);
        }
        else
        {
            var dialog = pageStack.push(Qt.resolvedUrl("SaveDialog.qml"));
            dialog.accepted.connect(function()
            {
                console.log("Saving workout");
                recorder.exportGpx(dialog.name, dialog.description);
                recorder.clearTrack();  // TODO: Make sure save was successful?

                //Mainpage must load history data to get this new workout in the list
                bLoadHistoryData = true;

                //We must return here to the mainpage.
                pageStack.pop(vMainPageObject, PageStackAction.Immediate);
            })
            dialog.rejected.connect(function()
            {
                console.log("Cancel workout");
                recorder.clearTrack();

                //We must return here to the mainpage.
                pageStack.pop(vMainPageObject, PageStackAction.Immediate);
            })
        }
    }

    Timer {
        id: updateTimer
        interval: 1000;
        running: true;
        repeat: true

        onTriggered: {
            timeView.update();
        }
    }

    Rectangle {
        anchors.fill: parent
        color: displayColors.background
    }


    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: page.height
        visible: !lockScreen.visible

        PullDownMenu
        {
            MenuItem
            {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsMenu.qml"))
            }
            MenuItem
            {
                text: qsTr("Switch Color Theme")
                onClicked:
                {
                    RecordPageColorTheme.nextTheme();
                    settings.displayMode = RecordPageColorTheme.id;
                }
            }
            MenuItem
            {
                text: mapView.visible ?  qsTr("Hide Map") :  qsTr("Show Map")
                onClicked:
                {
                    mapView.visible = !mapView.visible
                    settings.showMapRecordPage = mapView.visible;
                }
            }
        }


        PushUpMenu
        {
            MenuItem
            {
                text: qsTr("Lock Screen")
                onClicked: lockScreen.visible = !lockScreen.visible
            }
        }

        ColumnLayout {
            spacing: 0
            anchors.leftMargin: Theme.paddingMedium
            anchors.rightMargin: Theme.paddingMedium
            anchors.bottomMargin: Theme.paddingMedium
            anchors.fill: parent

            RowLayout {
                id: statusBar
                Layout.fillWidth: true
                Layout.columnSpan: 2
                spacing: Theme.paddingMedium

                GPSIndicator {
                    id: gps
                    Layout.fillWidth: true

                    height: Theme.itemSizeExtraSmall / 5 * 3
                    indicatorWidth: page.width / 5

                    accuracy: recorder.accuracy
                    accuracyThreshold: recorder.accuracyThreshold
                }

                Rectangle {
                    id: timeView

                    height: Theme.itemSizeExtraSmall / 5 * 3
                    color: "transparent"

                    function update() {
                        var date = new Date();
                        timeLabel.text = JSTools.fncPadZeros(date.getHours(), 2) + ":" + JSTools.fncPadZeros(date.getMinutes(), 2) + ":" + JSTools.fncPadZeros(date.getSeconds(), 2) + " ";
                    }

                    Text {
                        id: timeLabel
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeExtraSmall
                        color: displayColors.secondary

                        text: ""

                        Component.onCompleted: timeView.update()
                    }
                }

                Rectangle {
                    id: hrm
                    Layout.fillWidth: true

                    height: Theme.itemSizeExtraSmall / 5 * 3
                    color: "transparent"

                    Text {
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeExtraSmall
                        color: displayColors.secondary

                        text: qsTr("HRM off")
                    }
                }
            }

            Separator
            {
                Layout.fillWidth: true
                Layout.columnSpan: 2

                color: displayColors.secondaryHighlight
                horizontalAlignment: Qt.AlignHCenter
                visible: false
            }

            Item {
                id: mainView
                Layout.fillWidth: true
                Layout.fillHeight: true

                GridLayout {
                    anchors.fill: parent
                    columns: 2
                    rowSpacing: 0
                    columnSpacing: 0

                    RecordPageFieldLoader {
                        id: field_0_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_0_1.visible ? 1 : 2

                        row: 0
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_0_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_0_0.visible ? 1 : 2

                        row: 0
                        column: 1
                    }

                    Separator
                    {
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_1_0.visible || field_1_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_1_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_1_1.visible ? 1 : 2

                        row: 1
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_1_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_1_0.visible ? 1 : 2

                        row: 1
                        column: 1
                    }

                    Separator
                    {
                        id: separator
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_2_0.visible || field_2_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_2_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_2_1.visible ? 1 : 2

                        row: 2
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_2_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_2_0.visible ? 1 : 2

                        row: 2
                        column: 1
                    }

                    Separator
                    {
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_3_0.visible || field_3_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_3_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_3_1.visible ? 1 : 2

                        row: 3
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_3_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_3_0.visible ? 1 : 2

                        row: 3
                        column: 1
                    }
                }

                Rectangle {
                    id: mapView
                    width: page.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: mapAtTop ? parent.top : undefined
                    anchors.bottom: mapAtBottom ? parent.bottom : undefined
                    visible: settings.showMapRecordPage

                    states: [
                        State {
                            when: map.maximized
                            ParentChange { target: mapView; parent: page }
                            PropertyChanges { target: mapView; anchors.bottomMargin: 0; height: parent.height }
                        },
                        State {
                            when: !map.maximized
                            ParentChange { target: mapView; parent: mainView }
                            PropertyChanges { target: mapView; anchors.bottomMargin: Theme.paddingMedium; height: parent.height / 2 }
                        }
                    ]

                    //TODO: opacity transition on visibility change
                    //TODO: position transition on mouse release

                    property int moveStartPosition: 0
                    property bool moving: false

                    property bool mapAtTop: false
                    property bool mapAtBottom: true

                    readonly property int yMin: 0
                    readonly property int yMax: parent.height - height - Theme.paddingMedium
                    readonly property int yThreshold: yMin + (yMax - yMin) / 2

                    MouseArea {
                        anchors.fill: parent
                        preventStealing: true
                        onPressed: {
                            mapView.moving = true;
                            mapView.moveStartPosition = mouse.y;
                            mapView.mapAtTop = false;
                            mapView.mapAtBottom = false;
                        }
                        onReleased: {
                            mapView.moving = false;
                            if(mapView.y < mapView.yThreshold) {
                                mapView.mapAtTop = true;
                            } else {
                                mapView.mapAtBottom = true;
                            }
                        }
                        onMouseYChanged: {
                            if(mapView.moving) {
                                mapView.y = mapView.y - (mapView.moveStartPosition - mouse.y);
                                if (mapView.y < mapView.yMin) {
                                    mapView.y = mapView.yMin;
                                } else if (mapView.y > mapView.yMax) {
                                    mapView.y = mapView.yMax;
                                }
                            }
                        }
                    }

                    Map {
                        id: map
                        anchors.fill: parent

                        mapGesturesEnabled: map.maximized
                        showCenterButton: map.maximized

                        onShowSettings: pageStack.push(Qt.resolvedUrl("MapSettingsPage.qml"))
                    }
                }
            }

            CustomButton {
                id: startButton
                Layout.fillWidth: true

                backgroundColor: displayColors.startButton
                textColor: "white"

                text: qsTr("Start")

                onClicked: {
                    visible = !visible
                    recorder.running = true
                }
            }

            RowLayout {
                Layout.fillWidth: true
                spacing: Theme.paddingMedium
                visible: !startButton.visible

                CustomButton {
                    id: lapButton
                    Layout.fillWidth: true

                    backgroundColor: displayColors.lapButton
                    textColor: "white"

                    text: qsTr("Stage")
                }
                CustomButton {
                    id: pauseButton
                    Layout.preferredWidth: pauseButton.height

                    backgroundColor: recorder.pause ? displayColors.startButton : displayColors.pauseButton
                    textColor: "white"

                    fontSize: recorder.pause ? Theme.fontSizeLarge : Theme.fontSizeExtraLarge

                    text: recorder.pause ? "\u25B6" : "II"

                    onClicked: recorder.pause = !recorder.pause
                }
                CustomButton {
                    id: stopButton
                    Layout.fillWidth: true

                    slide: true
                    backgroundColor: displayColors.stopButton
                    textColor: "white"

                    text: qsTr("Stop")
                    textLeft: "🡅"
                    textRight: "🡅"

                    onClicked: showSaveDialog()
                }
            }
        }
    }

    RecordPageLockScreen {
        id: lockScreen
        anchors.fill: parent
        color: displayColors.lockScreen
        unlockButtonColor: displayColors.primary
        unlockButtonHighlightColor: displayColors.highlight
        visible: false
    }
}
