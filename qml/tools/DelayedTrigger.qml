import QtQuick 2.0
import QtQml 2.2

Item {
    id: root

    property string backgroundColor: "white"
    property int delay: 1000
    
    signal trigger()
    
    Rectangle {
        id: leftRect
        width: 1
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: middleRect.left
        
        visible: mouseArea.activePress
        
        color: root.backgroundColor
        
        PropertyAnimation {
            id: animationLeft;
            target: leftRect;
            property: "width";
            to: leftRect.x
            duration: root.delay
            easing.type: Easing.OutSine
            running: mouseArea.activePress
            
            onStopped: {
                if(mouseArea.activePress) {
                    mouseArea.preventStealing = false
                    mouseArea.initPress = false
                    root.trigger()
                }
            }
        }
    }
    
    Rectangle {
        id: middleRect
        width: 0
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        
        visible: mouseArea.activePress
    }
    
    Rectangle {
        id: rightRect
        width: 1
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: middleRect.right
        
        visible: mouseArea.activePress
        
        color: root.backgroundColor
        
        PropertyAnimation {
            id: animationRight;
            target: rightRect;
            property: "width";
            to: root.width - rightRect.x
            duration: root.delay
            easing.type: Easing.OutSine
            running: mouseArea.activePress
        }
    }
    
    MouseArea {
        id: mouseArea
        anchors.fill: parent

        property int pressStartX
        property int pressStartY
        property int maxDelta: 50

        property bool moveDetected: false
        property bool initPress: false
        property bool activePress: pressed && initPress

        onPressed: {
            leftRect.width = 0;
            rightRect.width = 0;
            middleRect.x = mouse.x;

            moveDetected = false
            initPress = false
            pressStartX = mouse.x
            pressStartY = mouse.y
        }

        onPressAndHold: {
            if(!moveDetected) {
                preventStealing = true
                initPress = !moveDetected
            }
        }

        onReleased: {
            preventStealing = false
            initPress = false
        }

        onMouseXChanged: {
            var delta = pressStartX - mouse.x
            if(delta < 0) { delta = delta * -1}
            moveDetected = moveDetected || delta > maxDelta
        }

        onMouseYChanged: {
            var delta = pressStartY - mouse.y
            if(delta < 0) { delta = delta * -1}
            moveDetected = moveDetected || delta > maxDelta
        }
    }
}
