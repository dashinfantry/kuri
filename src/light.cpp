#include "light.h"

#include <QDebug>

Light::Light(QObject* parent)
    : QObject(parent) {
    m_sensor = new QLightSensor(this);
    QObject::connect(m_sensor, SIGNAL(readingChanged()), this, SLOT(refresh()));
}

qreal Light::brightness(void) const {
    return m_brightness;
}

void Light::deactivate(void) {
    Q_ASSERT(m_sensor);

    // stop the sensor if all parts are deactivated
    if (m_sensor->isActive()) {
        qDebug() << "Sensor stopped";
        m_sensor->stop();
    }
}

void Light::refresh(void) {
    Q_ASSERT(m_sensor);

    if (!m_sensor->isActive()) {
        qDebug() << "Sensor started";
        m_sensor->setAlwaysOn(true);
        m_sensor->start();
    }

    QLightSensor*  light   = dynamic_cast<QLightSensor*>(m_sensor);
    QLightReading* reading = light->reading();

    m_brightness = reading->lux();

    emit brightnessChanged();
}
