#pragma once

#include <QColor>
#include <QtQuick/QQuickPaintedItem>

#include <numeric>

class PlotWidget : public QQuickPaintedItem {
    Q_OBJECT

    Q_PROPERTY(QColor plotColor READ plotColor WRITE setplotColor)
    Q_PROPERTY(QColor scaleColor READ scaleColor WRITE setscaleColor)
    Q_PROPERTY(unsigned scrollStep READ scrollStep WRITE setscrollStep)

    typedef QList<qreal> ValueList;

public:
    static const unsigned NUM_SCALE_LINES = 5; // number of lines in the background indicating the plot scale

    explicit PlotWidget(QQuickItem* parent = nullptr);

    void paint(QPainter* painter);

    void setplotColor(const QColor& color) { m_plotColor = color; }
    void setscaleColor(const QColor& color) { m_scaleColor = color; }
    void setscrollStep(unsigned step) { m_scrollStep = step; }

    const QColor& plotColor() { return m_plotColor; }
    const QColor& scaleColor() { return m_scaleColor; }
    unsigned      scrollStep() { return m_scrollStep; }

public slots:
    void addValue(qreal v);
    void reset(void);

private:
    QColor m_plotColor;
    QColor m_scaleColor;

    QList<qreal> m_values;

    qreal m_minValue {std::numeric_limits<qreal>::max()};
    qreal m_maxValue {std::numeric_limits<qreal>::lowest()};

    unsigned m_scrollStep {3};
};
